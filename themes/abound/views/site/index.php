<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#primium-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php
$gridDataProvider = new CArrayDataProvider(array(
    array('id' => 1, 'firstName' => 'Mark', 'lastName' => 'Otto', 'language' => 'CSS', 'usage' => '<span class="inlinebar">1,3,4,5,3,5</span>'),
    array('id' => 2, 'firstName' => 'Jacob', 'lastName' => 'Thornton', 'language' => 'JavaScript', 'usage' => '<span class="inlinebar">1,3,16,5,12,5</span>'),
    array('id' => 3, 'firstName' => 'Stu', 'lastName' => 'Dent', 'language' => 'HTML', 'usage' => '<span class="inlinebar">1,4,4,7,5,9,10</span>'),
    array('id' => 4, 'firstName' => 'Jacob', 'lastName' => 'Thornton', 'language' => 'JavaScript', 'usage' => '<span class="inlinebar">1,3,16,5,12,5</span>'),
    array('id' => 5, 'firstName' => 'Stu', 'lastName' => 'Dent', 'language' => 'HTML', 'usage' => '<span class="inlinebar">1,3,4,5,3,5</span>'),
));
?>

<div class="row-fluid">
    <div class="span3 ">
        <div class="stat-block">
            <a href="<?php echo Yii::app()->createUrl("primium/admin") ?>">
                <ul>
                    <li class="stat-graph " id="weekly-visit"><i class="fa fa-list fa-2x"></i></li>
                    <li class="stat-count"><span>Summary</span><span>All Record</span></li>
                    <li class="stat-percent"><span
                            class="text-success stat-percent"><?php echo($primium->search()->totalItemCount); ?></span>
                    </li>
                </ul>
            </a>

        </div>
    </div>
    <div class="span3 ">
        <div class="stat-block">
            <a href="<?php echo Yii::app()->createUrl("primium/first") ?>">
                <ul>
                    <li class="stat-graph" id="new-visits"><i class="fa fa-exclamation-triangle fa-2x"></i></li>
                    <li class="stat-count"><span>1st</span><span>Remider</span></li>
                    <li class="stat-percent"><span
                            class="text-error stat-percent"><?php echo($firstReminder->firstReminder()->totalItemCount); ?></span>
                    </li>
                </ul>
            </a>
        </div>
    </div>
    <div class="span3 ">
        <div class="stat-block">
            <a href="<?php echo Yii::app()->createUrl("primium/inquiry") ?>">
                <ul>
                    <li class="stat-graph" id="unique-visits"><i class="fa fa fa-question fa-2x"></i></li>
                    <li class="stat-count"><span>Inquiry </span> <span>Session </span></li>
                    <li class="stat-percent"><span class="text-success stat-percent"><?php echo($inquiryReminder->inquiryReminder()->totalItemCount); ?></span></li>
                </ul>
            </a>
        </div>
    </div>
    <div class="span3 ">
        <div class="stat-block">
            <a href="<?php echo Yii::app()->createUrl("primium/second") ?>">
                <ul>
                    <li class="stat-graph" id=""><i class="fa fa-quote-right fa-2x"></i></li>
                    <li class="stat-count"><span>2nd</span><span>Reminder</span></li>
                    <li class="stat-percent"><span><span class="text-success stat-percent"><?php echo($secondReminder->secondReminder()->totalItemCount); ?></span></li>
                </ul>
            </a>
        </div>
    </div>
</div>

<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '<b>AVTC Monitoring Report Summary</b>&nbsp;<a href="' . Yii::app()->createUrl("primium/create") . '" class="pull-right"><i class="icon icon-plus"></i>Add New</a>',
));

?>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $primium,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'primium-grid',
    'dataProvider' => $primium->search(),
    'filter' => $primium,
    'columns' => array(
        array(
            'name' => 'status',
            'value' => array($primium, 'renderStatus'),
        ),
        'year',
        'dls_reference',
        'date_approve',
        'affected_lot',
        'register_proprietor',
        /*
        'developer',
        'date_mosa_issue',
        'date_mosa_expired',
        'subdivided',
        'premium',
        'annual_rent',
        'preparation_title_fee',
        */
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{view}{delete}',
            'deleteConfirmation'=>"js:'Do you really want to delete record with DLS References '+$(this).parent().parent().children(':nth-child(3)').text()+'?'",
            'buttons' => array(
                'update' => array(
                    'visible' => 'true',
                    'url' => 'Yii::app()->controller->createUrl("primium/update",array("id"=>$data->id))',
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'delete' => array(
                    'visible' => 'true',
                    'url' => 'Yii::app()->controller->createUrl("primium/delete",array("id"=>$data->id))',
                )
            )

        ),
    ),
)); ?>
<?php
//$premiumStatus = array();
//$premiumStatus[0] = 'Unpaid';
//$premiumStatus[1] = 'Paid';
//Yii::app()->par->premiumStatus = $premiumStatus;
//$statusList = Yii::app()->par->premiumStatus;
//echo var_dump(unserialize($statusList));
//Yii::app()->par->purge('premiumStatus');
$this->endWidget();

//Yii::app()->par->test = '1234';//set parameter. If it is not present, it will be created
//Yii::app()->par->premiumStatus = array('0'=>'Unpaid','1'=>'Paid');
//echo var_dump(Yii::app()->par->premiumStatus);//output parameter. If it is not present, exception is thrown
?>

