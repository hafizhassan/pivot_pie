<div class="table-responsive">
  <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tr>
        <td colspan="2"><strong>SMA/SMU&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div style="clear: both;">TAHUN:
      <?php echo $form->textField($academic,'sma_year',array('class'=>'span1','maxlength'=>4)); ?></div></strong></td>
    <td colspan="2"><strong>SPM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div style="clear: both;">TAHUN:
      <?php echo $form->textField($academic,'spm_year',array('class'=>'span1','maxlength'=>4)); ?></div></strong></td>
    <td colspan="2"><strong>LAIN-LAIN<div style="clear: both;">Nyatakan:<?php echo $form->textField($academic,'other_state',array('class'=>'span2','maxlength'=>30)); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="clear: both;">TAHUN:
      <?php echo $form->textField($academic,'other_year',array('class'=>'span1','maxlength'=>4)); ?></div></strong></td>
  </tr>
  <tr>
    <td><p><strong>MATA PELAJARAN</strong></p></td>
    <td><strong>GRED</strong></td>
    <td><p><strong>MATA PELAJARAN</strong></p></td>
    <td><strong>GRED</strong></td>
    <td><p><strong>MATA PELAJARAN</strong></p></td>
    <td><strong>GRED</strong></td>
    </tr>
  <tr>
    <td>BAHASA ARAB</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'sma_arabic',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td>BAHASA MELAYU</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'spm_bm',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_1',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_1',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>TAFSIR &amp; HADIS</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'sma_tafsir',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td>BAHASA ARAB TINGGI / KOMUNIKASI</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'spm_high_arabic',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject2',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_2',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>TAUHID &amp; FEQH</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'sma_tauhid',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td>PENDIDIKAN SYARIAH ISLAMIAH</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'spm_syariah',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_3',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_3',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>AL-QURAN</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'sma_quran',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td>PENDIDIKAN AL-QURAN &amp; AS-SUNNAH</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'spm_quran',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_4',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_4',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>TASAWWUR ISLAM</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'spm_tasawwur',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_5',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_5',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>PENDIDIKAN</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'spm_pai',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_6',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_6',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_7',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_7',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_8',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_8',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_9',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_9',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_10',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_10',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_subject_11',array('class'=>'span2','maxlength'=>30)); ?></span></strong></td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_grade_11',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  <tr>
    <td>PANGKAT KESELURUHAN</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'sma_total',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td>PANGKAT KESELURUHAN</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'spm_total',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
    <td>PANGKAT KESELURUHAN</td>
    <td><strong><span style="clear: both;"><?php echo $form->textField($academic,'other_total',array('class'=>'span1','maxlength'=>4)); ?></span></strong></td>
  </tr>
  </table>
</div>