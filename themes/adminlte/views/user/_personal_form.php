
<br />
<br />
<p class="help-block"><?php echo Yii::t('strings','Fields with '); ?><span class="required">*</span>
<?php echo Yii::t('strings',' are required.'); ?></p>

<?php echo $form->errorSummary(array($user,$profile,$academic)); ?>
  

    <?php echo $form->textFieldRow($user,'username',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->hiddenField($user,'password',array('class'=>'span5','maxlength'=>45,'value'=>'123456')); ?>	
	<?php echo $form->textFieldRow($user,'email',array('class'=>'span3','maxlength'=>100)); ?>
	<?php echo $form->dropDownListRow($user,'user_type', $user->getUserOptions(), array('empty'=>Yii::t('strings','--Please Choose--'), 'user_type'=>'user_type')); ?>	
	<?php echo $form->textFieldRow($profile,'student_id',array('class'=>'span3','maxlength'=>20)); ?>
	<?php echo $form->textFieldRow($profile,'name',array('class'=>'span5','maxlength'=>100)); ?>
	<?php echo $form->textAreaRow($profile,'address',array('class'=>'span5','maxlength'=>100)); ?>
	<?php echo $form->textFieldRow($profile,'zipcode',array('class'=>'span2','maxlength'=>5)); ?>
	<?php echo $form->dropDownListRow($profile,'state', CHtml::listData(State::model()->findAll(), 'state_id', 'state'), array('empty'=>Yii::t('strings','--Please Choose--'), 'state'=>'state_id')); ?>		
	<?php //echo $form->textFieldRow($profile,'state',array('class'=>'span5','maxlength'=>100)); ?>
	<?php echo $form->textFieldRow($profile,'international_student',array('class'=>'span5','maxlength'=>100)); ?>
	<?php echo $form->radioButtonListRow(
            $profile,
            'maritul_status',
            array(
                Yii::t('strings', "Single"),
                Yii::t('strings', "Married"),
            )
        ); ?>
	<?php echo $form->dropDownListRow($profile,'race', CHtml::listData(Race::model()->findAll(), 'race_id', 'race'), array('empty'=>Yii::t('strings','--Please Choose--'), 'race'=>'race_id')); ?>		
	
	<?php echo $form->datepickerRow($profile, 'birth_date',array('hint'=>'','options'=>array('format' => 'yyyy-mm-dd', 
												//'showOtherMonths' => true,      // show dates in other months
												//'selectOtherMonths' => true,    // can seelect dates in other months
												'changeYear' => true,           // can change year
												'changeMonth' => true,          // can change month
												'yearRange' => '1920:2013',     // range of year
												//'minDate' => '2000-01-01',      // minimum date
												//'maxDate' => '2099-12-31', 
											      ),
									     'size'=>60,'maxlength'=>50, 'id'=>'birth_date')); ?>
        <?php echo $form->dropDownListRow($profile,'nationality', CHtml::listData(Country::model()->findAll(), 'country_code', 'country'), array('empty'=>Yii::t('strings','--Please Choose--'), 'nationality'=>'country_code')); ?>
	<?php echo $form->textFieldRow($profile,'no_tel',array('class'=>'span3','maxlength'=>100)); ?>
	<?php echo $form->textFieldRow($profile,'no_hp',array('class'=>'span3','maxlength'=>100)); ?>



