<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/tableSelect.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />

	<title>

	<?php echo CHtml::encode($this->pageTitle); 
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.charcounter.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.tableSelect.js');
        Yii::app()->clientScript->registerScript('limitText','$("#textAreaId").charCounter(160);');
        ?>
    </title>

</head>

<body>

<div class="container" id="page">

	<div id="header">
	<!--
	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/mablogo.gif" style="width: 15%; height: 15%">
            
    --><div id="language-selector" style="float:right; margin:5px;">
    <?php
    if(!Yii::app()->user->isGuest)
    {
    	$this->widget(
    'bootstrap.widgets.TbButtonGroup',
    array(
        'buttons' => array(
            array(
                'label' => Yii::app()->user->name,
                'items' => array(
                    array('label'=>Yii::t('strings','Logout'), 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    //array('label' => Yii::t('strings','Personal Informations'), 'url' => array('/userProfile/create'),'visible'=>!$this->currentUserProfile()!=""),
                    //array('label' => 'Something else', 'url' => '#'),
                    
                )
            ),
        ),
    )
	);
    }
	

	?>
                </div>
		<div id="logo"><?php echo CHtml::encode(Yii::t('strings','Al-Baghdadi Student Information Systems')); ?></div>
                
	</div><!-- header -->

	<div id="">
            <?php /* $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		));
                  */  $this->widget(
                                'bootstrap.widgets.TbNavbar',
                                array(
                                'brand' => '',
                                'fixed' => false,
                                'items' => array(
                                array(
                                'class' => 'bootstrap.widgets.TbMenu',
                                'items' => array(
				array('label'=>Yii::t('strings','Home'), 'url'=>array('/site/index')),
				array('label'=>Yii::t('strings','Login'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),)))));
                  ?>
		
	</div><!-- mainmenu -->
	<?php $this->widget('application.extensions.flash.Flash', array(
		'keys'=>array('success','error'),
		'htmlOptions'=>array('class'=>'flash'),
	)); ?><!-- flashes -->

	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	)); ?><!-- breadcrumbs -->

	<?php echo $content; ?>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Al-Baghdadi Sdn Bhd.<br />
		All Rights Reserved.<br/>
                <div id="language-selector" style="float:right; margin:5px;">
                    <?php 
                    $this->widget('application.components.widgets.LanguageSelector');
                    ?>
                </div>    
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>