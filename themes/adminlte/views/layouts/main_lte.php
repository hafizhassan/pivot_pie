<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>AdminLTE | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>
    <!-- font Awesome -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- Morris chart -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris/morris.css" rel="stylesheet" type="text/css"/>
    <!-- jvectormap -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet"
          type="text/css"/>
    <!-- fullCalendar -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fullcalendar/fullcalendar.css" rel="stylesheet"
          type="text/css"/>
    <!-- Daterange picker -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/daterangepicker/daterangepicker-bs3.css"
          rel="stylesheet" type="text/css"/>
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/loading.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="index.html" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        Yii Template
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <?php
        if (!Yii::app()->user->isGuest) {
            ?>

            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-right">

                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="">You have 4 messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/avatar3.png"
                                                     class="img-circle" alt="User Image"/>
                                            </div>
                                            <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                </ul>
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li>
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-warning"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu  pull-right">
                            <li class="">You have 10 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="ion ion-ios7-people info"></i> 5 new members joined today
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks: style can be found in dropdown.less -->
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tasks"></i>
                            <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="">You have 9 tasks</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Design some buttons
                                                <small class="pull-right">20%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-aqua" style="width: 20%"
                                                     role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                     aria-valuemax="100">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->

                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i>
                            <span><?php echo Yii::app()->user->name ?> <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <!-- User image -->
                            <li class="user-header bg-light-blue">
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/avatar3.png"
                                     class="img-circle" alt="User Image"/>

                                <p>
                                    Jane Doe - Web Developer
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo Yii::app()->createUrl('site/logout') ?>"
                                       class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        <?php } ?>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php
    if (!Yii::app()->user->isGuest) {
        ?>
        <aside class="left-side sidebar-offcanvas">
            <!-- sidebar: style can be found in sidebar.less -->

            <section class="sidebar">

                <!-- Sidebar user panel -->
                <div class="user-panel" id="manageSession">
                    <?php if(isset(Yii::app()->user->manageSession)){?>
                    <div class="pull-left image"><p><?php echo Yii::t('application','Currently Manage:');?></p>
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/avatar3.png" class="img-circle"
                             alt="User Image"/>
                    </div>
                    <div class="pull-left info">
                        <p id="manageSession"><?php if(isset(Yii::app()->user->manageSession)) echo  Yii::app()->user->manageSession?></p>

                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                    <?php }//end if?>
                </div>
                <div id="floatingCirclesG" style="display: none;">
                    <div class="f_circleG" id="frotateG_01">
                    </div>
                    <div class="f_circleG" id="frotateG_02">
                    </div>
                    <div class="f_circleG" id="frotateG_03">
                    </div>
                    <div class="f_circleG" id="frotateG_04">
                    </div>
                    <div class="f_circleG" id="frotateG_05">
                    </div>
                    <div class="f_circleG" id="frotateG_06">
                    </div>
                    <div class="f_circleG" id="frotateG_07">
                    </div>
                    <div class="f_circleG" id="frotateG_08">
                    </div>
                </div>
                <!-- search form -->



                <form action="<?php echo $this->createUrl('./employee')?>" method="get" class="sidebar-form">

                <div class="input-group">

                    <?php
                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'name' => 'employee',
                        'source' => $this->createUrl('user/autocompleteTest'),
                        // additional javascript options for the autocomplete plugin
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'class' => 'form-control',
                            'placeholder'=>Yii::t('application','Search employee')
                            //'style'=>'height:32px;',
                        ),
                    ));
                    ?>

                    <span class="input-group-btn">
                         <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
               <i class="fa-"></i>

                            </span>
                </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="active">
                        <a href="<?php echo Yii::app()->createUrl('site/index') ?>">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/pages/widgets.html">
                            <i class="fa fa-th"></i> <span>Widgets</span>
                            <small class="badge pull-right bg-green">new</small>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-bar-chart-o"></i>
                            <span>Charts</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/pages/charts/morris.html"><i
                                        class="fa fa-angle-double-right"></i> Morris</a></li>
                            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/pages/charts/flot.html"><i
                                        class="fa fa-angle-double-right"></i> Flot</a></li>
                            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/pages/charts/inline.html"><i
                                        class="fa fa-angle-double-right"></i> Inline charts</a></li>
                        </ul>
                    </li>

                </ul>

            </section>


        </aside>
        <!-- /.sidebar --><?php } ?>
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <?php $this->widget('application.extensions.flash.Flash', array(
                'keys' => array('success', 'error'),
                'htmlOptions' => array('class' => 'flash'),
            )); ?><!-- flashes -->

            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => $this->breadcrumbs,
            )); ?>
        </section>

        <?php echo $content; ?>
    </aside>
    <!-- /.right-side -->

</div>
<!-- ./wrapper -->
<div id="footer">

    <div id="language-selector" style="float:right; margin:5px;">
        <?php
        $this->widget('application.components.widgets.LanguageSelector');
        ?>
    </div>
</div>
<!-- footer -->
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php //echo CHtml::encode($this->pageTitle);
//Yii::app()->clientScript->registerCoreScript('jquery');
// Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery-ui-1.10.3.min.js');

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.charcounter.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.tableSelect.js');
/*jQuery UI 1.10.3*/

/*Bootstrap*/
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/bootstrap.min.js');
/*Morris.js charts*/
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/morris/morris.min.js');
/*Sparkline*/
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/sparkline/jquery.sparkline.min.js');
/*jvectormap*/
/*fullCalendar*/
/*jQuery Knob Chart*/
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/plugins/jqueryKnob/jquery.knob.js');
/*daterangepicker*/
/* Bootstrap WYSIHTML5 */
/*iCheck*/
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/AdminLTE/app.js');
Yii::app()->clientScript->registerScript('limitText', '$("#textAreaId").charCounter(160);');
?>


</body>
</html>