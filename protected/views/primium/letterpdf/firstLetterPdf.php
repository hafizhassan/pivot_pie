<table width="100%" class="table" >
    <tr>
        <td width="17%"><img src="images/kenyalang_logo.jpg" width="120" height="120" />

        </td>
        <td width="66%" class="text-center" style="text-align: center;"><strong align="center"> JABATAN TANAH DAN SURVEI</strong><br>
            <strong>BAHAGIAN MIRI</strong><br>
            Wisma Pelita Tunku, Jalan Puchong, 98000 Miri, Sarawak, Malaysia<br>
            Telefon: 085-435000, 435001,435002     Faks: 085-418191<br>
            Laman Web: www.landsurvey.sarawak.gov.my E-mel: landsurvey@sarawak.net.gov.my
            </td>
        <td width="17%"><div align="center"><img src="images/tanah_survei_sarawak.jpg" width="160" height="160" /></div></td>
    </tr>
</table>
<div align="left"></div>


<hr />

    <table width="100%">
        <tr>
            <td width="72%">Ruj Kami :&nbsp;<b><?php echo ($model->dls_reference);?></b></td>
            <td width="28%">Tarikh:&nbsp;<b><?php echo date_format(date_create($model->date_approve), "d.m.Y");?></b></td>
        </tr>
        <tr>
            <td><p>Ruj Tuan :&nbsp;<b><?php echo ($model->developer);?>,&nbsp;<?php echo ($model->developer_address);?></b></p></td>
            <td>Tarikh:&nbsp;<b><?php echo date("d.m.Y");?></b></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <p>Tuan,</p>
    <table width="100%" height="312" border="0.5">
        <tr>
            <td><p><strong>Per: TUNTUTAN PEMBAYARAN PREMIUM TERTUNGGAK (AVTC/SUBDIVISION) UNTUK : &nbsp;<b><?php echo ($model->affected_lot);?></strong></p><br>
                <p>Lanjutan kepada surat kelulusan ruj. <?php echo ($model->dls_reference);?> bertarikh <?php echo date_format(date_create($model->date_approve), "d.m.Y");?> didapati syarikat tuan masih belum menjelaskan premium sebanyak RM <?php echo ($model->premium);?>.</p><br>
                <p>2.   Oleh yang demikian, tuan dikehendaki untuk menjelaskan jumlah tertunggak berkenaan dalam tempoh 30 hari daripada tarikh surat ini.</p><br>
                <p>3. Jika tuan gagal berbuat demikian, tindakan di bawah boleh di ambil terhadapat syarikat tuan:</p><br>
                <blockquote>
                    <p>(i). Segala urusan yang melibatkan pendataran suratcara ke atas tanah tersebut akan ditolak selaras dengan peruntuan <strong>Seksyen 122 Kanun Tanah Negeri Sarawak</strong>.</p><br>

                </blockquote>

                <p>4.  Untuk makluman tuan, kegagalan membayar premium tertunggak boleh menyebabkan segala urusan syarikat tuan dengan pejabat ini dibekukan sehingga pembayaran akhir diselesaikan.</p><br>
                <p>5. Sekiranya syarikat tuan memerlukan penjelasan lanjut, sila berhubung terus dengan pejabat ini.</p><br>
                <p>&nbsp;</p></td>
        </tr>
        <tr>
            <td><p align="center"><strong>&quot;BERSATU BERUSAHA BERBAKTI&quot;</strong></p>
                <p align="center"><strong>&quot;<em>AN HONOUR TO SERVE</em>&quot;</strong></p></td>
        </tr>
        <tr>
            <td><p>&nbsp;</p>
                <p align="center">(<strong>ROZLAN BIN HAJI PUTIT</strong>)</p>
                <p align="center">Penguasa Tanah Dan Survei,</p>
                <p align="center">Bahagian Miri</p>
                <p>&nbsp;</p></td>
        </tr>
        <tr>
            <td>Pegawai untuk dihubungi: (remark)</td>
        </tr>
        <tr>
            <td><p>&nbsp;</p>
                <p>**Cetakan ini adalah cetakan berkomputer, dan tidak memerlukan tandatangan.</p></td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p align="center">&nbsp;</p>
    <p align="center"><em>An Agency to Facilitate Development</em><em></em></p>

