<?php
$baseUrl = Yii::app()->baseUrl;
$imagePath = $baseUrl . "/images/";
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile($baseUrl.'/js/yourscript.js');
$cs->registerCssFile($baseUrl . '/css/afour.css');
?>

<table width="100%" border="0.5">
    <tr>
        <td width="17%"><img src="<?php echo $imagePath; ?>kenyalang_logo.jpg" width="160" height="160"/></td>
        <td width="66%" class="text-center" style="text-align: center;"><strong align="center"> JABATAN TANAH DAN SURVEI</strong><br>
            <strong>BAHAGIAN MIRI</strong><br>
            Wisma Pelita Tunku, Jalan Puchong, 98000 Miri, Sarawak, Malaysia<br>
            Telefon: 085-435000, 435001,435002     Faks: 085-418191<br>
            Laman Web: www.landsurvey.sarawak.gov.my E-mel: landsurvey@sarawak.net.gov.my
        </td>
        <td width="17%">
            <div align="center"><img src="<?php echo $imagePath; ?>tanah_survei_sarawak.jpg" width="164" height="154"/>
            </div>
        </td>
    </tr>
</table>
<div align="left"></div>


<hr/>

<table width="100%" border="0.5">
    <tr>
        <td width="72%">Ruj Kami :&nbsp;<b><?php echo($model->dls_reference); ?></b></td>
        <td width="28%">Tarikh:&nbsp;<b><?php echo($model->date_approve); ?></b></td>
    </tr>
    <tr>
        <td><p>&nbsp;</p></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
<p>Dear Sir/Madam</p>


<p><strong>Re: INQUIRY ON OUTSTANDING OF LAND PREMIUM IN RESPECT OF: &nbsp;<b><?php echo($model->affected_lot); ?>
    </strong></p>
<p>You are hereby kindly invited to attend a discussion on the above subject matter.</p>
<p>2.&nbsp;&nbsp; You or your representative presence is hereby sought to be present at the venue and time as stated below:</p>
<div class="tab">
    <p>Date:  &nbsp;<b><?php echo($model->dateInquiry($model)); ?></b><br>
        Time: &nbsp;<b><?php echo($model->inquiry_time); ?></b><br>
        Venue:&nbsp;<b><?php echo($model->inquiry_venue); ?></b></p>

</div>


<p align="center"><strong>&quot;BERSATU BERUSAHA BERBAKTI&quot;</strong><br>
    <strong>&quot;<em>AN HONOUR TO SERVE</em>&quot;</strong></p>


<p>&nbsp;</p>
<p align="center">(<strong>DOUGLAS PUNGGA ANAK LAWANG</strong>)</p>
<p align="center">b.p Penguasa Tanah Dan Survei,</p>
<p align="center">Bahagian Miri</p>
<p>&nbsp;</p>


Pegawai untuk dihubungi: &nbsp;<b><?php echo($model->officer); ?></b>


<p>&nbsp;</p>
<p>**Cetakan ini adalah cetakan berkomputer, dan tidak memerlukan tandatangan.</p>


<p align="center">&nbsp;</p>
<p align="center"><em>An Agency to Facilitate Development</em><em></em></p>



