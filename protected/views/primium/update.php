<?php
/* @var $this PrimiumController */
/* @var $model Primium */

$this->breadcrumbs=array(
	'Premium'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Premium', 'url'=>array('index')),
	array('label'=>'Create Premium', 'url'=>array('create')),
	array('label'=>'View Premium', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Premium', 'url'=>array('admin')),
);
?>

<h1>Update Premium <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>