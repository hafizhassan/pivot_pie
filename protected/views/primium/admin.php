<?php
/* @var $this PrimiumController */
/* @var $model Primium */

$this->breadcrumbs=array(
	'Premium'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Premium', 'url'=>array('index')),
	array('label'=>'Create Premium', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#primium-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Premium</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'primium-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'year',
		'dls_reference',
		'date_approve',
		'affected_lot',
		'register_proprietor',
		/*
		'developer',
		'date_mosa_issue',
		'date_mosa_expired',
		'subdivided',
		'premium',
		'annual_rent',
		'preparation_title_fee',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
