<?php
/* @var $this PrimiumController */
/* @var $model Primium */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'year'); ?>
		<?php echo $form->textField($model,'year'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dls_reference'); ?>
		<?php echo $form->textField($model,'dls_reference',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_approve'); ?>
		<?php echo $form->textField($model,'date_approve'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'affected_lot'); ?>
		<?php echo $form->textArea($model,'affected_lot',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'register_proprietor'); ?>
		<?php echo $form->textArea($model,'register_proprietor',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'developer'); ?>
		<?php echo $form->textArea($model,'developer',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_mosa_issue'); ?>
		<?php echo $form->textField($model,'date_mosa_issue'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_mosa_expired'); ?>
		<?php echo $form->textField($model,'date_mosa_expired'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'subdivided'); ?>
		<?php echo $form->textField($model,'subdivided'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'premium'); ?>
		<?php echo $form->textField($model,'premium',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'annual_rent'); ?>
		<?php echo $form->textField($model,'annual_rent',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'preparation_title_fee'); ?>
		<?php echo $form->textField($model,'preparation_title_fee',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->