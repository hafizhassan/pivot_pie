<?php
/* @var $this PrimiumController */
/* @var $model Primium */
/* @var $form CActiveForm */
?>

<div class="row-fluid">

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'primium-form',

    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'horizontal',
    ),
));
?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model,"Please fix following;","Error",array("class"=>"alert alert-error")); ?>
<div class="span5">
    <?php
    $this->beginWidget('zii.widgets.CPortlet', array(
        'title' => "",
    ));

    ?>
    <?php echo $form->labelEx($model, 'year'); ?>
    <?php
    $yearNow = date("Y");
    $yearFrom = $yearNow - 100;
    $yearTo = $yearNow + 4;
    $arrYears = array();

    foreach (range($yearFrom, $yearTo) as $number) {
        $arrYears[$number] = $number;
    }

    $arrYears = array_reverse($arrYears, true);
    echo $form->dropDownList($model, 'year', $arrYears,array('options' => array($yearNow=>array('selected'=>true))));
    // echo $form->textField($model,'year');
    ?>
    <?php echo $form->error($model, 'year', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'dls_reference'); ?>
    <?php echo $form->textField($model, 'dls_reference', array('size' => 60, 'maxlength' => 256)); ?>
    <?php echo $form->error($model, 'dls_reference', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'date_approve'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $model,
        'attribute' => 'date_approve',
        'name' => 'date_approve',
        // 'flat'=>true,//remove to hide the datepicker
        'options' => array(
            'dateFormat' => 'yy-mm-dd', // save to db format
            'altField' => '#date_approve',
            'altFormat' => 'dd MM yy', // show to user format
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'changeMonth' => true,
            'changeYear' => true,
            'yearRange' => '2000:2099',
            'minDate' => '2000-01-01', // minimum date
            'maxDate' => '2099-12-31', // maximum date
        ),
        'htmlOptions' => array(
            'style' => ''
        ),
    ));
    ?>

    <?php echo $form->error($model, 'date_approve', array("class" => "alert alert-error")); ?>

    <?php echo $form->labelEx($model, 'affected_lot'); ?>
    <?php echo $form->textArea($model, 'affected_lot', array('rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->error($model, 'affected_lot', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'register_proprietor'); ?>
    <?php echo $form->textArea($model, 'register_proprietor', array('rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->error($model, 'register_proprietor', array("class" => "alert alert-error")); ?>


    <?php echo $form->labelEx($model, 'status'); ?>
    <?php

    $statusList = Yii::app()->par->premiumStatus;
    $statusList = unserialize($statusList);
    //  print_r(array_keys($statusList));
    //echo json_encode(key($statusList));
    echo $form->dropDownList($model, 'status', $statusList);
    // echo $form->textField($model,'year');
    ?>
    <?php echo $form->error($model, 'status', array("class" => "alert alert-error")); ?>
    <br/>





    <?php $this->endWidget(); ?>
</div>
<div class="span5">
    <?php
    $this->beginWidget('zii.widgets.CPortlet', array(
        'title' => "",
    ));

    ?>
    <?php echo $form->labelEx($model, 'developer'); ?>
    <?php echo $form->textArea($model, 'developer', array('rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->error($model, 'developer', array("class" => "alert alert-error")); ?>

    <?php echo $form->labelEx($model, 'developer_address'); ?>
    <?php echo $form->textArea($model, 'developer_address', array('rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->error($model, 'developer_address', array("class" => "alert alert-error")); ?>

    <?php echo $form->labelEx($model, 'date_mosa_issue'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $model,
        'attribute' => 'date_mosa_issue',
        'name' => 'date_mosa_issue',
        // 'flat'=>true,//remove to hide the datepicker
        'options' => array(
            'dateFormat' => 'yy-mm-dd', // save to db format
            'altField' => '#date_mosa_issue',
            'altFormat' => 'dd MM yy', // show to user format
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'changeMonth' => true,
            'changeYear' => true,
            'yearRange' => '2000:2099',
            'minDate' => '2000-01-01', // minimum date
            'maxDate' => '2099-12-31', // maximum date
        ),
        'htmlOptions' => array(
            'style' => ''
        ),
    ));

    //echo $form->textField($model,'date_mosa_issue');
    ?>
    <?php echo $form->error($model, 'date_mosa_issue', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'date_mosa_expired'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model' => $model,
        'attribute' => 'date_mosa_expired',
        'name' => 'date_mosa_expired',
        // 'flat'=>true,//remove to hide the datepicker
        'options' => array(
            'dateFormat' => 'yy-mm-dd', // save to db format
            'altField' => '#date_mosa_expired',
            'altFormat' => 'dd MM yy', // show to user format
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'changeMonth' => true,
            'changeYear' => true,
            'yearRange' => '2000:2099',
            'minDate' => '2000-01-01', // minimum date
            'maxDate' => '2099-12-31', // maximum date
        ),
        'htmlOptions' => array(
            'style' => ''
        ),
    ));

    //echo $form->textField($model,'date_mosa_expired');
    ?>
    <?php echo $form->error($model, 'date_mosa_expired', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'subdivided'); ?>
    <?php echo $form->textField($model, 'subdivided'); ?>
    <?php echo $form->error($model, 'subdivided', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'premium'); ?>
    <?php echo $form->textField($model, 'premium', array('size' => 10, 'maxlength' => 10)); ?>
    <?php echo $form->error($model, 'premium', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'annual_rent'); ?>
    <?php echo $form->textField($model, 'annual_rent', array('size' => 10, 'maxlength' => 10)); ?>
    <?php echo $form->error($model, 'annual_rent', array("class" => "alert alert-error")); ?>



    <?php echo $form->labelEx($model, 'preparation_title_fee'); ?>
    <?php echo $form->textField($model, 'preparation_title_fee', array('size' => 10, 'maxlength' => 10)); ?>
    <?php echo $form->error($model, 'preparation_title_fee', array("class" => "alert alert-error")); ?>



    <?php $this->endWidget(); ?>
</div>

<div class="span10 portlet ">

    <table data-toggle="table" data-url="data1.json" data-cache="false" data-height="299">
        <tbody>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'date_issue'); ?>
            </td>
            <td data-field="id">
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    //'id' => 'date_issue',
                    'attribute' => 'date_issue',
                    'name' => 'date_issue',
                    // 'flat'=>true,//remove to hide the datepicker
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd', // save to db format
                        'altField' => '#date_issue',
                        'altFormat' => 'dd MM yy', // show to user format
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '2000:2099',
                        'minDate' => '2000-01-01', // minimum date
                        'maxDate' => '2099-12-31', // maximum date
                    ),
                    'htmlOptions' => array(
                        'style' => ''
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'date_issue_inquiry', array("class" => "alert alert-error")); ?>
            </td>
            <td data-field="name">until</td>
            <td data-field="price"><span
                    id="date_issue_until"><?php echo date('d.m.Y', strtotime($model->date_issue . "+30 days")); ?></span>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'date_issue_inquiry'); ?>
            </td>
            <td>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'date_issue_inquiry',
                    'name' => 'date_issue_inquiry',
                    // 'flat'=>true,//remove to hide the datepicker
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd', // save to db format
                        'altField' => '#date_issue_inquiry',
                        'altFormat' => 'dd MM yy', // show to user format
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '2000:2099',
                        'minDate' => '2000-01-01', // minimum date
                        'maxDate' => '2099-12-31', // maximum date
                    ),
                    'htmlOptions' => array(
                        'style' => ''
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'date_issue_inquiry', array("class" => "alert alert-error")); ?>
            </td>
            <td>
                until
            </td>
            <td>
                <span
                    id="date_issue_until_inquiry"><?php echo date('d.m.Y', strtotime($model->date_issue_inquiry . "+30 days")); ?></span>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'date_issue_second'); ?>
            </td>
            <td>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'date_issue_second',
                    'name' => 'date_issue_second',
                    // 'flat'=>true,//remove to hide the datepicker
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd', // save to db format
                        'altField' => '#date_issue_second',
                        'altFormat' => 'dd MM yy', // show to user format
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '2000:2099',
                        'minDate' => '2000-01-01', // minimum date
                        'maxDate' => '2099-12-31', // maximum date
                    ),
                    'htmlOptions' => array(
                        'style' => ''
                    ),
                ));
                ?>

                <?php echo $form->error($model, 'date_issue_second', array("class" => "alert alert-error")); ?>
            </td>
            <td>
                until
            </td>
            <td>
                <span
                    id="date_issue_until_second"><?php echo date('d.m.Y', strtotime($model->date_issue_second . "+30 days")); ?></span>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->







    <div class="portlet-content text-center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save', array("class"=>"btn btn-success")); ?>
        <?php echo $model->isNewRecord ? '':CHtml::submitButton('Paid', array("class"=>"btn btn-primary","id"=>"paidBtn")); ?>
    </div>

</div>


<?php $this->endWidget(); //end of form widget?>
<?php
Yii::app()->clientScript->registerScript('paid', "
$('#paidBtn').click(function(e){
   // e.preventDefault();

	$('#Primium_status').val(1);
	//return false;
});
");

?>

<script type="text/javascript">

    (function ($, window, document, undefined) {


        $("#date_issue").on("change", function () {
            var date = new Date($("#date_issue").val());
            if (!isNaN(date.getTime())) {
                date.setDate(date.getDate() + 30);

                $("#date_issue_until").text(date.toInputFormat());
            } else {
                alert("Invalid Date");
            }
        });

        //$("#date_issue_until_inquiry").text($("#date_issue_inquiry").val());
        $("#date_issue_inquiry").on("change", function () {
            var date = new Date($("#date_issue_inquiry").val());
            if (!isNaN(date.getTime())) {
                date.setDate(date.getDate() + 15);

                $("#date_issue_until_inquiry").text(date.toInputFormat());
            } else {
                alert("Invalid Date");
            }
        });

        //$("#date_issue_until_second").text($("#date_issue_second").val());
        $("#date_issue_second").on("change", function () {
            var date = new Date($("#date_issue_second").val());
            if (!isNaN(date.getTime())) {
                date.setDate(date.getDate() + 30);

                $("#date_issue_until_second").text(date.toInputFormat());
            } else {
                alert("Invalid Date");
            }
        });


        //From: http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
        Date.prototype.toInputFormat = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = this.getDate().toString();
            return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // padding
        };
    })(jQuery, this, document);
</script>
</div><!-- form -->