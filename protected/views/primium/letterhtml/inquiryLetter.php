<?php
$baseUrl = Yii::app()->baseUrl;
$imagePath = $baseUrl . "/images/";
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile($baseUrl.'/js/yourscript.js');
$cs->registerCssFile($baseUrl . '/css/afour.css');
?>

<?php
echo CHtml::image($baseUrl . '/images/pdficon_small.png');
echo CHtml::link('Download Pdf', array('primium/inquiryLetterPdf?id=' . $model->id)); ?><i
    class="icon icon-download-alt"></i><br>
<hr>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'primium-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
)); ?>
<table width="100%" border="0.5">
    <tr>
        <td width="17%"><img src="<?php echo $imagePath; ?>kenyalang_logo.jpg" width="160" height="160"/></td>
        <td width="66%"><p align="center">&nbsp;</p>

            <p align="center"><strong>JABATAN TANAH DAN SURVEI</strong></p>

            <p align="center"><strong>BAHAGIAN MIRI</strong></p>

            <p align="center">Wisma Pelita Tunku, Jalan Puchong, 98000 Miri, Sarawak, Malaysia</p>

            <p align="center">Telefon: 085-435000, 435001,435002 Faks: 085-418191</p>

            <p align="center">Laman Web: www.landsurvey.sarawak.gov.my E-mel: landsurvey@sarawak.net.gov.my</p></td>
        <td width="17%">
            <div align="center"><img src="<?php echo $imagePath; ?>tanah_survei_sarawak.jpg" width="164" height="154"/>
            </div>
        </td>
    </tr>
</table>
<div align="left"></div>


<hr/>

<table width="100%" border="0.5">
    <tr>
        <td width="72%">Ruj Kami :&nbsp;<b><?php echo($model->dls_reference); ?></b></td>
        <td width="28%">Tarikh:&nbsp;<b><?php echo($model->date_approve); ?></b></td>
    </tr>
    <tr>
        <td><p>&nbsp;</p></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
<p>Dear Sir/Madam</p>
<table width="100%" height="312" border="0.5">
    <tr>
        <td><p><strong>Re: INQUIRY ON OUTSTANDING OF LAND PREMIUM IN RESPECT OF:
                    &nbsp;<?php echo($model->affected_lot); ?></strong></p>

            <p>You are hereby kindly invited to attend a discussion on the above subject matter.</p>

            <p>2. You or your representative presence is hereby sought to be present at the venue and time as stated
                below:</p>

            <div class="tab">
                <p>Date: <?php echo ($model->dateInquiry($model)); ?></p>

                <p>Time: <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                    $this->widget('CJuiDateTimePicker', array(
                        'model' => $model, //Model object
                        'attribute' => 'inquiry_time', //attribute name
                        'mode' => 'time', //use "time","date" or "datetime" (default)
                        'options' => array(
                            //'timeFormat'=>strtolower(Yii::app()->locale->timeFormat),
                            'controlType' => 'select',
                            'ampm'=> true,
                            //'amNames'=>array('AM'=>'Pagi'),
                            //'pmNames'=>array('PM'=>'Ptg'),
                        ) ,// jquery plugin options

                    ));
                    ?></p>

                <p>
                    Venue:<?php echo $form->textArea($model, 'inquiry_venue', array('rows' => 2, 'cols' => 50, 'value' => $model->inquiry_venue)); ?></p>
            </div>


        </td>
    </tr>
    <tr>
        <td><p align="center"><strong>&quot;BERSATU BERUSAHA BERBAKTI&quot;</strong></p>

            <p align="center"><strong>&quot;<em>AN HONOUR TO SERVE</em>&quot;</strong></p></td>
    </tr>
    <tr>
        <td><p>&nbsp;</p>

            <p align="center">(<strong>DOUGLAS PUNGGA ANAK LAWANG</strong>)</p>

            <p align="center">b.p Penguasa Tanah Dan Survei,</p>

            <p align="center">Bahagian Miri</p>

            <p>&nbsp;</p></td>
    </tr>
    <tr>
        <td>Pegawai untuk dihubungi: <?php echo $form->textField($model, 'officer', array('size' => 60, 'maxlength' => 256)); ?></td>
    </tr>
    <tr>
        <td><p>&nbsp;</p>

            <p>**Cetakan ini adalah cetakan berkomputer, dan tidak memerlukan tandatangan.</p></td>
    </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p align="center">&nbsp;</p>
<p align="center"><em>An Agency to Facilitate Development</em><em></em></p>
<p>&nbsp;</p>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save', array("class" => "btn btn-success")); ?>
<?php $this->endWidget(); //end of form widget?>
