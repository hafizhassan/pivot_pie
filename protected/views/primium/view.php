<?php
/* @var $this PrimiumController */
/* @var $model Primium */

$this->breadcrumbs=array(
	'Premium'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Premium', 'url'=>array('index')),
	array('label'=>'Create Premium', 'url'=>array('create')),
	array('label'=>'Update Premium', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Premium', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Premium', 'url'=>array('admin')),
);
?>

<h1>View Premium #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'year',
		'dls_reference',
		'date_approve',
		'affected_lot',
		'register_proprietor',
		'developer',
		'date_mosa_issue',
		'date_mosa_expired',
		'subdivided',
		'premium',
		'annual_rent',
		'preparation_title_fee',
	),
)); ?>
