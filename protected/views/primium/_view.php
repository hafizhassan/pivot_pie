<?php
/* @var $this PrimiumController */
/* @var $data Primium */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dls_reference')); ?>:</b>
	<?php echo CHtml::encode($data->dls_reference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_approve')); ?>:</b>
	<?php echo CHtml::encode($data->date_approve); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('affected_lot')); ?>:</b>
	<?php echo CHtml::encode($data->affected_lot); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('register_proprietor')); ?>:</b>
	<?php echo CHtml::encode($data->register_proprietor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('developer')); ?>:</b>
	<?php echo CHtml::encode($data->developer); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('date_mosa_issue')); ?>:</b>
	<?php echo CHtml::encode($data->date_mosa_issue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_mosa_expired')); ?>:</b>
	<?php echo CHtml::encode($data->date_mosa_expired); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subdivided')); ?>:</b>
	<?php echo CHtml::encode($data->subdivided); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('premium')); ?>:</b>
	<?php echo CHtml::encode($data->premium); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('annual_rent')); ?>:</b>
	<?php echo CHtml::encode($data->annual_rent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('preparation_title_fee')); ?>:</b>
	<?php echo CHtml::encode($data->preparation_title_fee); ?>
	<br />

	*/ ?>

</div>