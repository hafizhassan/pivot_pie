<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('strings','Login');
$this->breadcrumbs=array(
	Yii::t('strings','Login'),
);
?>
<div class="form">
    <section class="col-lg-5 ">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
        'htmlOptions'=>array(
            'class'=>'lockscreen',
        ),
    )); ?>
    <div class="box box-primary">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title"><?php echo Yii::t('strings','Login'); ?></h3>
            <!-- tools box -->

        </div>
        <div class="box-body">
            <form action="#" method="post">
                <div class="row">
                    <?php echo $form->labelEx($model,'username'); ?>
                    <?php echo $form->textField($model,'username'); ?>
                    <?php echo $form->error($model,'username'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model,'password'); ?>
                    <?php echo $form->passwordField($model,'password',array('class'=>' lockscreen-credentials')); ?>
                    <?php echo $form->error($model,'password'); ?>
                </div>
                <div class="row remember">
                    <?php echo $form->checkBox($model,'rememberMe'); ?>
                    <?php echo $form->label($model,'rememberMe'); ?>
                    <?php echo $form->error($model,'rememberMe'); ?>
                </div>

            </form>
        </div>
        <div class="box-footer clearfix">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Log masuk', 'type'=>'success')); ?>
        </div>

    </div>
        <?php $this->endWidget(); ?>
    </section>
</div>