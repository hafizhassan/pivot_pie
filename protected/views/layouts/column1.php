<?php $this->beginContent('application.views.layouts.main_lte'); ?>

<div class="container">
<!--	<div id="content"> -->
    <!-- Main content -->
    <section class="content">
        <?php echo $content; ?>
    </section><!-- /.content -->

    <!--	</div> content-->
</div>
<?php $this->endContent(); ?>