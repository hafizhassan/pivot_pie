<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . Yii::t('application', 'Change password');
$this->breadcrumbs=array(
    Yii::t('strings','Change Password'),
);
?>
<div class="form">
    <section class="col-lg-5 ">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'user-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
            'htmlOptions'=>array(
                'class'=>'lockscreen',
            ),
        )); ?>
        <div class="box box-primary">
            <div class="box-header">
                <i class="fa fa-key"></i>

                <h2 class="box-title"><?php echo Yii::t('application', 'Change Password') ?></h2>

            </div>
            <div class="box-body">
                <?php
                $this->widget('bootstrap.widgets.TbAlert', array(
                        'block' => true, // display a larger alert block?
                        'fade' => true, // use transitions?
                        'closeText' => '&times;', // close link text - if set to false, no close link is displayed
                        'alerts' => array( // configurations per alert type
                            'success' => array(
                                'block' => true,
                                'fade' => true,
                                'closeText' => '&times;',
                            ), // success, info, warning, error or danger
                        ),
                    )
                );
                ?>


                <?php //echo $form->passwordFieldRow($model, 'currentPassword', array('class' => 'span3')); ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'currentPassword'); ?>
                    <?php echo $form->passwordField($model,'currentPassword',array('size'=>45,'maxlength'=>45,'class'=>'span3 form-control ')); ?>
                    <?php echo $form->error($model,'currentPassword',array('class'=>'label alert-danger')); ?>
                </div>
                <div class="row">
                    <?php //echo $form->passwordFieldRow($model, 'newPassword', array('class' => 'span3')); ?>
                    <?php echo $form->labelEx($model,'newPassword'); ?>
                    <?php echo $form->passwordField($model,'newPassword',array('size'=>45,'maxlength'=>45,'class'=>'span3 form-control ')); ?>
                    <?php echo $form->error($model,'newPassword',array('class'=>'label alert-danger')); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($model,'newPassword_repeat'); ?>
                    <?php echo $form->passwordField($model,'newPassword_repeat',array('size'=>45,'maxlength'=>45,'class'=>'span3 form-control ')); ?>
                    <?php echo $form->error($model,'newPassword_repeat',array('class'=>'label alert-danger')); ?>
                </div>
            </div>
            <div class="box-footer clearfix">
                <?php echo CHtml::submitButton(Yii::t('application', 'Submit'),array('class'=>'btn btn-primary btn-sm btn-flat')); ?>
                <?php //$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => Yii::t('application', 'Submit'), 'type' => 'primary')); ?>
                <?php $this->endWidget(); ?>

            </div>
        </div>
    </section>
</div>
