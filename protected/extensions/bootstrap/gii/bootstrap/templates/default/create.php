<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('modelLabels', '$label')=>array('index'),
	Yii::t('strings', 'Create'),
);\n";
?>

$this->menu=array(
array('label'=>Yii::t('strings', 'List ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('index')),
array('label'=>Yii::t('strings', 'Manage ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('admin')),
);
?>

<h1><?php echo "<?php echo Yii::t('strings', 'Create ')?>";?><?php echo "<?php echo Yii::t('modelLabels', '".$this->modelClass."'); ?>"; ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
