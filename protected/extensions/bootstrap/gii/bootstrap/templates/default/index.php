<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('modelLabels', '$label'),
);\n";
?>

$this->menu=array(
array('label'=>Yii::t('strings', 'Create ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('create')),
array('label'=>Yii::t('strings', 'Manage ').<?php echo "Yii::t('modelLabels', '".$this->modelClass."')"; ?>,'url'=>array('admin')),
);
?>

<h1><?php echo "<?php echo Yii::t('modelLabels', '".$label."'); ?>"; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
