<?php

class EmployeeController extends Controller
{
	public function actionIndex()
	{
        $res = array();
        if(isset($_GET['employee'])){
            //$user = User::model()->findByAttributes(array('username'=>));
            $qtxt ="SELECT username FROM yii_user WHERE username = :username";
            $command =Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":username", ''.$_GET['employee'].'', PDO::PARAM_STR);
            $res =$command->queryColumn();
        }
        if(count($res)!== 0){
            $this->render('index',array('employee'=>$res));
        } else {
            $this->render('no_employee');
        }


	}

	// Uncomment the following methods and override them if needed

	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'rights',

		);
	}
/*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}