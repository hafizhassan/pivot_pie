<?php

class PrimiumController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','first','firstletter', 'firstletterpdf',
                    'second','secondletter', 'secondletterpdf',
                    'inquiry','inquiryletter', 'inquiryletterpdf'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Primium;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Primium']))
		{
			$model->attributes=$_POST['Primium'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Primium']))
		{
			$model->attributes=$_POST['Primium'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Primium');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Primium('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Primium']))
			$model->attributes=$_GET['Primium'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    public function actionFirst(){
        $model=new Primium('firstReminder');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Primium']))
            $model->attributes=$_GET['Primium'];

        $this->render('firstReminder',array(
            'model'=>$model,
        ));
    }
    public function actionFirstletter(){
        $model=Primium::model()->findByPk((int) $_GET['id']);


        $this->render('letterhtml/firstLetter',array(
            'model'=>$model,
        ));
    }
    public function actionFirstletterpdf(){
        $model=Primium::model()->findByPk((int) $_GET['id']);
        // mPDF
        /*$html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('firstLetterPdf', array('model'=>$model), true));
        $html2pdf->Output();*/
        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');

        # render (full page)
        //$mPDF1->WriteHTML($this->render('index', array(), true));

        # Load a stylesheet
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/print.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        # renderPartial (only 'view' of current controller)
        $mPDF1->WriteHTML($this->renderPartial('letterpdf/firstLetterPdf', array('model'=>$model), true));

        # Renders image
        //$mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));

        # Outputs ready PDF
        $mPDF1->Output('firstLetter-'.$model->developer.'-'.date("d.m.Y-His").'.pdf', 'D');
    }

    public function actionInquiry(){
        $model=new Primium('inquiryReminder');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Primium']))
            $model->attributes=$_GET['Primium'];

        $this->render('inquiryReminder',array(
            'model'=>$model,
        ));
    }
    public function actionInquiryletter($id){
        //$model=Primium::model()->findByPk((int) $_GET['id']);
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Primium']))
        {
            $model->attributes=$_POST['Primium'];
            if($model->save())
                $this->redirect(array('primium/inquiryLetter','id'=>$model->id));
        }

        $this->render('letterhtml/inquiryLetter',array(
            'model'=>$model,
        ));
    }
    public function actionInquiryletterpdf(){
        $model=Primium::model()->findByPk((int) $_GET['id']);
        // mPDF
        /*$html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('firstLetterPdf', array('model'=>$model), true));
        $html2pdf->Output();*/
        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');

        # render (full page)
        //$mPDF1->WriteHTML($this->render('index', array(), true));

        # Load a stylesheet
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/print.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        # renderPartial (only 'view' of current controller)
        $mPDF1->WriteHTML($this->renderPartial('letterpdf/inquiryLetterPdf', array('model'=>$model), true));

        # Renders image
        //$mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));

        # Outputs ready PDF
        $mPDF1->Output('inquiryLetter-'.$model->developer.'-'.date("d.m.Y-His").'.pdf', 'D');
    }

    public function actionSecond(){
        $model=new Primium('secondReminder');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Primium']))
            $model->attributes=$_GET['Primium'];

        $this->render('secondReminder',array(
            'model'=>$model,
        ));
    }
    public function actionSecondletter(){
        $model=Primium::model()->findByPk((int) $_GET['id']);


        $this->render('letterhtml/secondLetter',array(
            'model'=>$model,
        ));
    }
    public function actionSecondletterpdf(){
        $model=Primium::model()->findByPk((int) $_GET['id']);
        // mPDF
        /*$html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('firstLetterPdf', array('model'=>$model), true));
        $html2pdf->Output();*/
        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');

        # render (full page)
        //$mPDF1->WriteHTML($this->render('index', array(), true));

        # Load a stylesheet
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/print.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        # renderPartial (only 'view' of current controller)
        $mPDF1->WriteHTML($this->renderPartial('letterpdf/secondLetterPdf', array('model'=>$model), true));

        # Renders image
        //$mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));

        # Outputs ready PDF
        $mPDF1->Output('secondLetter-'.$model->developer.'-'.date("d.m.Y-His").'.pdf', 'D');
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Primium the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Primium::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Primium $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='primium-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
