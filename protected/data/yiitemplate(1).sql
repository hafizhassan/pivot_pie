-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2014 at 07:12 AM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yiitemplate`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Yii::t("modelLabels", "User ID")',
  `username` varchar(45) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Username")',
  `password` varchar(245) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Password")',
  `email` varchar(245) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Email")',
  `last_login_time` datetime DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Last Login Time")',
  `create_user_id` int(11) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Creator ID")',
  `create_time` datetime DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Create Time")',
  `update_user_id` int(11) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Updator ID")',
  `update_time` datetime DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Update Time")',
  `user_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yiiright_authassignment`
--

CREATE TABLE IF NOT EXISTS `yiiright_authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yiiright_authassignment`
--

INSERT INTO `yiiright_authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('Admin', '1', NULL, 'N;'),
('Admin', '2', NULL, 'N;'),
('Authenticated', '2', NULL, 'N;'),
('Comment.*', '2', NULL, 'N;'),
('Comment.Approve', '2', NULL, 'N;'),
('Editor', '2', NULL, 'N;'),
('Guest', '2', NULL, 'N;'),
('Stalker', '33', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `yiiright_authitem`
--

CREATE TABLE IF NOT EXISTS `yiiright_authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yiiright_authitem`
--

INSERT INTO `yiiright_authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Admin', 2, NULL, NULL, 'N;'),
('Authenticated', 2, 'Authenticated user', NULL, 'N;'),
('Comment.*', 1, 'Access all comment actions', NULL, 'N;'),
('Comment.Approve', 0, 'Approve comments', NULL, 'N;'),
('Comment.Delete', 0, 'Delete comments', NULL, 'N;'),
('Comment.Update', 0, 'Update comments', NULL, 'N;'),
('CommentAdministration', 1, 'Administration of comments', NULL, 'N;'),
('Editor', 2, 'Editor', NULL, 'N;'),
('Guest', 2, 'Guest user', NULL, 'N;'),
('Hello.*', 1, NULL, NULL, 'N;'),
('Hello.Index', 0, NULL, NULL, 'N;'),
('Post.*', 1, 'Access all post actions', NULL, 'N;'),
('Post.Admin', 0, 'Administer posts', NULL, 'N;'),
('Post.Create', 0, 'Create posts', NULL, 'N;'),
('Post.Delete', 0, 'Delete posts', NULL, 'N;'),
('Post.Update', 0, 'Update posts', NULL, 'N;'),
('Post.View', 0, 'View posts', NULL, 'N;'),
('PostAdministrator', 1, 'Administration of posts', NULL, 'N;'),
('PostUpdateOwn', 0, 'Update own posts', 'return Yii::app()->user->id==$params["userid"];', 'N;'),
('Site.*', 1, NULL, NULL, 'N;'),
('Site.Contact', 0, NULL, NULL, 'N;'),
('Site.Error', 0, NULL, NULL, 'N;'),
('Site.Index', 0, NULL, NULL, 'N;'),
('Site.Login', 0, NULL, NULL, 'N;'),
('Site.Logout', 0, NULL, NULL, 'N;'),
('Site.Restore', 0, NULL, NULL, 'N;'),
('Stalker', 2, 'Stalker', NULL, 'N;'),
('User.*', 1, NULL, NULL, 'N;'),
('User.Admin', 0, NULL, NULL, 'N;'),
('User.Create', 0, NULL, NULL, 'N;'),
('User.Delete', 0, NULL, NULL, 'N;'),
('User.Index', 0, NULL, NULL, 'N;'),
('User.Register', 0, NULL, NULL, 'N;'),
('User.Update', 0, NULL, NULL, 'N;'),
('User.View', 0, NULL, NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `yiiright_authitemchild`
--

CREATE TABLE IF NOT EXISTS `yiiright_authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yiiright_authitemchild`
--

INSERT INTO `yiiright_authitemchild` (`parent`, `child`) VALUES
('Editor', 'Authenticated'),
('CommentAdministration', 'Comment.*'),
('Editor', 'CommentAdministration'),
('Authenticated', 'CommentUpdateOwn'),
('Authenticated', 'Guest'),
('PostAdministrator', 'Post.*'),
('PostAdministrator', 'Post.Admin'),
('Authenticated', 'Post.Create'),
('PostAdministrator', 'Post.Create'),
('PostAdministrator', 'Post.Delete'),
('PostAdministrator', 'Post.Update'),
('Guest', 'Post.View'),
('Editor', 'PostAdministrator'),
('Authenticated', 'PostUpdateOwn'),
('Stalker', 'User.Index');

-- --------------------------------------------------------

--
-- Table structure for table `yiiright_rights`
--

CREATE TABLE IF NOT EXISTS `yiiright_rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yiiright_rights`
--

INSERT INTO `yiiright_rights` (`itemname`, `type`, `weight`) VALUES
('Authenticated', 2, 1),
('Editor', 2, 0),
('Guest', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `yii_user`
--

CREATE TABLE IF NOT EXISTS `yii_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Yii::t("modelLabels", "User ID")',
  `username` varchar(45) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Username")',
  `password` varchar(245) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Password")',
  `email` varchar(245) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Email")',
  `last_login_time` datetime DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Last Login Time")',
  `create_user_id` int(11) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Creator ID")',
  `create_time` datetime DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Create Time")',
  `update_user_id` int(11) DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Updator ID")',
  `update_time` datetime DEFAULT NULL COMMENT 'Yii::t("modelLabels", "Update Time")',
  `user_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `yii_user`
--

INSERT INTO `yii_user` (`id`, `username`, `password`, `email`, `last_login_time`, `create_user_id`, `create_time`, `update_user_id`, `update_time`, `user_type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, '2014-08-17 13:11:52', NULL, NULL, NULL, NULL, NULL),
(33, '12345', '21232f297a57a5a743894a0e4a801fc3', 'apa.oww@gmail.com', '2014-05-20 09:44:09', 1, '2014-05-16 16:05:24', 1, '2014-05-20 01:28:47', 0),
(34, 'admin2', '21232f297a57a5a743894a0e4a801fc3', '', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `yiiright_authassignment`
--
ALTER TABLE `yiiright_authassignment`
  ADD CONSTRAINT `yiiright_authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `yiiright_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
