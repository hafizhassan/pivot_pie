<?php

/**
 * This is the model class for table "pvt_primium".
 *
 * The followings are the available columns in table 'pvt_primium':
 * @property integer $id
 * @property integer $year
 * @property string $dls_reference
 * @property string $date_approve
 * @property string $affected_lot
 * @property string $register_proprietor
 * @property string $developer
 * @property string $date_mosa_issue
 * @property string $date_mosa_expired
 * @property integer $subdivided
 * @property string $premium
 * @property string $annual_rent
 * @property string $preparation_title_fee
 */
class Primium extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Primium the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */

    public function tableName()
    {
        return 'pvt_primium';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('year, dls_reference, date_approve, affected_lot, register_proprietor, developer, developer_address,  subdivided, premium, annual_rent, preparation_title_fee', 'required'),
            array('year, subdivided, status', 'numerical', 'integerOnly' => true),
            //array('date_issue', 'type', 'type' => 'date', 'message' => '{attribute}: is not a date!', 'dateFormat' => 'yyyy-mm-dd'),
            array('dls_reference,date_issue, date_issue_inquiry, date_issue_second, inquiry_time,inquiry_venue,officer,date_mosa_issue, date_mosa_expired,', 'length', 'max' => 256),
            array('premium, annual_rent, preparation_title_fee', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, year, dls_reference, date_approve, affected_lot, register_proprietor, developer, date_mosa_issue, date_mosa_expired, subdivided, premium, annual_rent, preparation_title_fee,status', 'safe', 'on' => 'search'),
            array('id, year, dls_reference, date_approve, affected_lot, register_proprietor, developer, date_mosa_issue, date_mosa_expired, subdivided, premium, annual_rent, preparation_title_fee,status', 'safe', 'on' => 'firstReminder'),
            array('id, year, dls_reference, date_approve, affected_lot, register_proprietor, developer, date_mosa_issue, date_mosa_expired, subdivided, premium, annual_rent, preparation_title_fee,status', 'safe', 'on' => 'inquiryReminder'),
            array('id, year, dls_reference, date_approve, affected_lot, register_proprietor, developer, date_mosa_issue, date_mosa_expired, subdivided, premium, annual_rent, preparation_title_fee,status', 'safe', 'on' => 'secondReminder'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('application', 'ID'),
            'year' => Yii::t('application', 'Year'),
            'dls_reference' => Yii::t('application', 'DLS Reference'),
            'date_approve' => Yii::t('application', 'Date Approve'),
            'affected_lot' => Yii::t('application', 'Affected Lot'),
            'register_proprietor' => Yii::t('application', 'Register Proprietor'),
            'developer' => Yii::t('application', 'Developer'),
            'developer_address' => Yii::t('application', 'Developer Address'),
            'date_mosa_issue' => Yii::t('application', 'Date MOSA Issue'),
            'date_mosa_expired' => Yii::t('application', 'Date MOSA Expired'),
            'subdivided' => Yii::t('application', 'Subdivided'),
            'premium' => Yii::t('application', 'Premium'),
            'annual_rent' => Yii::t('application', 'Annual Rent'),
            'preparation_title_fee' => Yii::t('application', 'Preparation Title Fee'),
            'date_issue' => Yii::t('application', 'Date Issued'),
            'date_issue_inquiry' => Yii::t('application', 'Date Issued Inquiry'),
            'date_issue_second' => Yii::t('application', 'Date Issued 2nd Reminder'),
            'status'=>Yii::t('application', 'Status'),
            'inquiry_time'=>Yii::t('application', 'Inquiry Time'),
            'inquiry_venue'=>Yii::t('application', 'Inquiry Venue'),
            'officer'=>Yii::t('application', 'Officer'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('year', $this->year);
        $criteria->compare('dls_reference', $this->dls_reference, true);
        $criteria->compare('date_approve', $this->date_approve, true);
        $criteria->compare('affected_lot', $this->affected_lot, true);
        $criteria->compare('register_proprietor', $this->register_proprietor, true);
        $criteria->compare('developer', $this->developer, true);
        $criteria->compare('date_mosa_issue', $this->date_mosa_issue, true);
        $criteria->compare('date_mosa_expired', $this->date_mosa_expired, true);
        $criteria->compare('subdivided', $this->subdivided);
        $criteria->compare('premium', $this->premium, true);
        $criteria->compare('annual_rent', $this->annual_rent, true);
        $criteria->compare('preparation_title_fee', $this->preparation_title_fee, true);
        $criteria->compare('date_issue', $this->date_issue, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function firstReminder()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.


        $sql = "t.*, DATE_ADD(t.date_issue,INTERVAL 30 DAY) as lastDay1stRemider";

        $criteria = new CDbCriteria;
        $criteria->select = $sql;
        $criteria->compare('id', $this->id);
        $criteria->compare('year', $this->year);
        $criteria->compare('dls_reference', $this->dls_reference, true);
        $criteria->compare('date_approve', $this->date_approve, true);
        $criteria->compare('affected_lot', $this->affected_lot, true);
        $criteria->compare('register_proprietor', $this->register_proprietor, true);
        $criteria->compare('developer', $this->developer, true);
        $criteria->compare('date_mosa_issue', $this->date_mosa_issue, true);
        $criteria->compare('date_mosa_expired', $this->date_mosa_expired, true);
        $criteria->compare('subdivided', $this->subdivided);
        $criteria->compare('premium', $this->premium, true);
        $criteria->compare('annual_rent', $this->annual_rent, true);
        $criteria->compare('preparation_title_fee', $this->preparation_title_fee, true);
        $criteria->compare('status', 0, true);
        $criteria->having = 'CURDATE() BETWEEN date_issue AND lastDay1stRemider';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria
        ));
    }

    public function inquiryReminder()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.


        $sql = "t.*, DATE_ADD(t.date_issue,INTERVAL 30 DAY) as lastDay1stRemider,  DATE_ADD(t.date_issue,INTERVAL 45 DAY) as lastDayInquiryRemider";

        $criteria = new CDbCriteria;
        $criteria->select = $sql;
        $criteria->compare('id', $this->id);
        $criteria->compare('year', $this->year);
        $criteria->compare('dls_reference', $this->dls_reference, true);
        $criteria->compare('date_approve', $this->date_approve, true);
        $criteria->compare('affected_lot', $this->affected_lot, true);
        $criteria->compare('register_proprietor', $this->register_proprietor, true);
        $criteria->compare('developer', $this->developer, true);
        $criteria->compare('date_mosa_issue', $this->date_mosa_issue, true);
        $criteria->compare('date_mosa_expired', $this->date_mosa_expired, true);
        $criteria->compare('subdivided', $this->subdivided);
        $criteria->compare('premium', $this->premium, true);
        $criteria->compare('annual_rent', $this->annual_rent, true);
        $criteria->compare('preparation_title_fee', $this->preparation_title_fee, true);
        $criteria->compare('status', 0, true);
        $criteria->having = 'CURDATE() BETWEEN lastDay1stRemider AND lastDayInquiryRemider';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria
        ));
    }

    public function secondReminder()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.


        $sql = "t.*, DATE_ADD(t.date_issue,INTERVAL 75 DAY) as lastDaySecondRemider,  DATE_ADD(t.date_issue,INTERVAL 45 DAY) as lastDayInquiryRemider";

        $criteria = new CDbCriteria;
        $criteria->select = $sql;
        $criteria->compare('id', $this->id);
        $criteria->compare('year', $this->year);
        $criteria->compare('dls_reference', $this->dls_reference, true);
        $criteria->compare('date_approve', $this->date_approve, true);
        $criteria->compare('affected_lot', $this->affected_lot, true);
        $criteria->compare('register_proprietor', $this->register_proprietor, true);
        $criteria->compare('developer', $this->developer, true);
        $criteria->compare('date_mosa_issue', $this->date_mosa_issue, true);
        $criteria->compare('date_mosa_expired', $this->date_mosa_expired, true);
        $criteria->compare('subdivided', $this->subdivided);
        $criteria->compare('premium', $this->premium, true);
        $criteria->compare('annual_rent', $this->annual_rent, true);
        $criteria->compare('preparation_title_fee', $this->preparation_title_fee, true);
        $criteria->compare('status', 0, true);
        $criteria->having = 'CURDATE() BETWEEN lastDayInquiryRemider AND lastDaySecondRemider';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria
        ));
    }

    public function renderStatus($data)
    {
        $statusList = Yii::app()->par->premiumStatus;
        if (!is_array($statusList)) {
            $statusList = unserialize($statusList);
        }
        if (!is_array($statusList)) {
            // something went wrong, initialize to empty array
            $statusList = array();
        }
        return $statusList[$data->status];
    }

    public function dateInquiry($data)
    {

        $date = $data->date_issue;
        $replaceDate = str_replace('-', '/', $date);
        $dateInquiry = date('d.m.Y', strtotime($replaceDate . "+31 days"));
        return $dateInquiry;
    }

    protected function beforeSave()
    {
        $this->date_approve = date('Y-m-d', strtotime($this->date_approve));
        $this->date_mosa_issue = date('Y-m-d', strtotime($this->date_mosa_issue));
        $this->date_mosa_expired = date('Y-m-d', strtotime($this->date_mosa_expired));
        $this->date_issue = date('Y-m-d', strtotime($this->date_issue));
        $this->date_issue_inquiry = date('Y-m-d', strtotime($this->date_issue_inquiry));
        $this->date_issue_second = date('Y-m-d', strtotime($this->date_issue_second));
        return TRUE;
    }

    protected function afterFind()
    {
        $this->date_approve = date('d.m.Y', strtotime($this->date_approve));
        $this->date_mosa_issue = date('d.m.Y', strtotime($this->date_mosa_issue));
        $this->date_mosa_expired = date('d.m.Y', strtotime($this->date_mosa_expired));
        $this->date_issue = date('d.m.Y', strtotime($this->date_issue));
        $this->date_issue_inquiry = date('d.m.Y', strtotime($this->date_issue_inquiry));
        $this->date_issue_second = date('d.m.Y', strtotime($this->date_issue_second));
        $this->inquiry_time = date('h:i A', strtotime($this->inquiry_time));
        return TRUE;
    }
}
