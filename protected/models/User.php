<?php

/**
 * This is the model class for table "yii_user".
 *
 * The followings are the available columns in table 'yii_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $last_login_time
 * @property integer $create_user_id
 * @property string $create_time
 * @property integer $update_user_id
 * @property string $update_time
 * @property integer $user_type
 */
class User extends CActiveRecord
{
    const USER_ADMIN=0;
    const USER_STUDENT=1;

    public $new_password;
    public $new_password_repeat;
    public $old_password;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'yii_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id, update_user_id, user_type', 'numerical', 'integerOnly' => true),
            array('username', 'length', 'max' => 45),
            array('password, email', 'length', 'max' => 245),
            array('last_login_time, create_time, update_time', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, password, email, last_login_time, create_user_id, create_time, update_user_id, update_time, user_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('application', 'ID'),
            'username' => Yii::t('application', 'Username'),
            'password' => Yii::t('application', 'Password'),
            'email' => Yii::t('application', 'Email'),
            'last_login_time' => Yii::t('application', 'Last Login Time'),
            'create_user_id' => Yii::t('application', 'Create User'),
            'create_time' => Yii::t('application', 'Create Time'),
            'update_user_id' => Yii::t('application', 'Update User'),
            'update_time' => Yii::t('application', 'Update Time'),
            'user_type' => Yii::t('application', 'User Type'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('last_login_time', $this->last_login_time, true);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_user_id', $this->update_user_id);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('user_type', $this->user_type);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    /**
     * Apply a hash on the password before we store it in the database
     */

    protected function afterValidate()
    {
        parent::afterValidate();
        if(!$this->hasErrors())
            $this->password = $this->hashPassword($this->password);
    }

    /**
     * Generates the password hash
     * @param string password
     * @return string hash
     */
    public function hashPassword($password)
    {
        return hash('md5',$password);
    }

    /**
     * Check if the given password is correct.
     * @param string the password to be validated
     * @return boolen whether the password is valid
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password)===$this->password;
    }
    public function equalPasswords($attribute, $params)
    {
        $user = User::model()->findByPk(Yii::app()->user->id);
        if ($user->password != $this->hashPassword($this->old_password))
            $this->addError($attribute, 'Kata laluan lama tidak tepat.');
    }

    /**
     * Retrieve list of user type
     * @return array of user type
     */
    public function getUserOptions()
    {
        return array(
            self::USER_ADMIN=>Yii::t('modelLabels', 'Admin'),
            self::USER_STUDENT=>Yii::t('modelLabels', 'Student'),
        );
    }

    /**
     * @return string of current user type

    public function getUserText()
    {
    $userOptions = $this->userOptions;
    return isset($userOptions[$this->user_type]) ? $userOptions[$this->user_type] : Yii::t('errorMessages', 'Unknown user type.');
    } */
}
