<?php

class ChangePasswordForm extends CFormModel
{
    public $currentPassword;
    public $newPassword;
    public $newPassword_repeat;
    private $_user;

    public function rules()
    {
        return array(
            array(
                'currentPassword', 'compareCurrentPassword'
            ),
            array(
                'currentPassword, newPassword, newPassword_repeat', 'required',
                'message'=>Yii::t('application','Please complete  {attribute}.'),
            ),
            array(
                'newPassword_repeat', 'compare',
                'compareAttribute'=>'newPassword',
                'message'=>Yii::t('application','The new password does not match.'),
            ),

        );
    }

    public function compareCurrentPassword($attribute,$params)
    {
        if( md5($this->currentPassword) !== $this->_user->password )
        {
            $this->addError($attribute,Yii::t('application',"Old password didn't match."));
        }
    }

    public function init()
    {
        $this->_user = User::model()->findByAttributes( array( 'id'=>Yii::app()->user->id ) );
    }

    public function attributeLabels()
    {
        return array(
            'currentPassword'=>Yii::t('application','Old password'),
            'newPassword'=>Yii::t('application','New password'),
            'newPassword_repeat'=>Yii::t('application','Repeat new password'),
        );
    }

    public function changePassword()
    {
        $this->_user->password = $this->newPassword;
        if( $this->_user->save() )
            return true;
        return false;
    }
}